package multimodule.service;

import multimodule.domain.Movie;
import multimodule.domain.MovieDTO;
import multimodule.domain.MovieMapper;
import multimodule.domain.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.webservices.WebServicesProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@org.springframework.stereotype.Service
@EnableConfigurationProperties(WebServicesProperties.class)
public class Service {
    @Autowired
    MovieRepository movieRepository;
    MovieMapper movieMapper = new MovieMapper();

    public MovieDTO createMovie(MovieDTO movieDTO){
        Movie library = movieMapper.fromDTO(movieDTO);
        return movieMapper.fromModel(movieRepository.save(library));
    }
}
