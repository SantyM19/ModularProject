package multimodule.domain;

public class MovieMapper {
    public Movie fromDTO(MovieDTO movieDTO) {
        Movie movie = new Movie();
        movie.setId(movieDTO.getId());
        movie.setNombre(movieDTO.getNombre());
        movie.setTipo(movieDTO.getTipo());
        return movie;
    }

    public MovieDTO fromModel(Movie library) {
        MovieDTO movieDTO = new MovieDTO();
        movieDTO.setId(library.getId());
        movieDTO.setNombre(library.getNombre());
        movieDTO.setTipo(library.getTipo());
        return movieDTO;
    }
}
